extends Node


const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")

# сигнал загрузки объектов
signal load_objekto()


var load_scene : String
var id_direktebla_query # под каким номером запроса отправили запрос на управляемый список
var id_direktebla_kosmo_query # под каким номером запроса отправили запрос на управляемый список в космосе
var id_objekto# id запроса на объекты
var id_ligilo = 0 # id запроса на   выход из станции/заход в станцию


func _ready():
	var config = ConfigFile.new()
	var err = config.load("res://settings.cfg")
	if err == OK:
		Global.server = config.get_value("global", "server", false)
		Global.autoload = config.get_value("global", "autoload", false)
		Global.logs = config.get_value("global", "logs", false)
	if Global.logs:
		print('запуск res://kerno/menuo/menuo.tscn')
# warning-ignore:return_value_discarded
	Net.connect("connection_failed", self, "_on_connection_failed")
# warning-ignore:return_value_discarded
	Net.connect("connection_succeeded", self, "_on_connection_success")
# warning-ignore:return_value_discarded
	Net.connect("server_disconnected", self, "_on_server_disconnect")
# warning-ignore:return_value_discarded
	Net.connect("input_data", self, "_on_data_start")


func go_realeco():
	if Global.realeco == 2:
		on_com()
	elif Global.realeco == 1:
		on_real()
	elif Global.realeco == 3:
		on_cap()


# Обработчик сигнала "connection_succeeded"
func _on_connection_success():
	var q = QueryObject.new()
	id_direktebla_query = Net.get_current_query_id()
	Net.send_json(q.get_direktebla_json(Net.statuso_laboranta, Net.tasko_tipo_objekto, id_direktebla_query))


# закрываем признаки загрузки данных
func fermo_signo(): # закрыть признак
	# если ещё загрузки не было, то и отключать нечего
	if not Global.loading:
		return
	Global.loading = false
	#отключаем перехват сигнала
	Net.disconnect("input_data", self, "_on_data")
	Net.data_server.clear()
# warning-ignore:return_value_discarded
	Net.connect("input_data", self, "_on_data_start")
	# снимаем признаки загрузки параллельных миров
	for obj in Global.direktebla_objekto:
		obj['kosmo'] = false


# Обработчик сигнала "connection_failed"
func _on_connection_failed():
	fermo_signo()
	if !Global.server:
		_on_real_pressed()
	pass


# Обработчик сигнала "server_disconnected"
func _on_server_disconnect():
	fermo_signo()
	if !Global.server:
		_on_real_pressed()
	pass

# обработчик сигнала прихода данных при старте программы
func _on_data_start():
	var i = 0
	for on_data in Net.data_server:
		# ответ на запрос списка управляемый объектов
		if on_data['id'] == String(id_direktebla_query):
			for objekt in on_data['payload']['data']['universoObjekto']['edges']:
				Global.direktebla_objekto[objekt['node']['realeco']['objId']-2]=objekt['node'].duplicate(true)
				Global.direktebla_objekto[objekt['node']['realeco']['objId']-2]['kosmo'] = false
			# теперь загружаем те объекты, которые из представленных находятся в космосе
			var q = QueryObject.new()
			id_direktebla_kosmo_query = Net.current_query_id
			Net.current_query_id += 1
			Net.send_json(q.get_direktebla_kosmo_json(id_direktebla_kosmo_query))
			#обработали запрос и удалили обработанную запись
			Net.data_server.remove(i)
		# список управляемый объектов в космосе
		elif on_data['id'] == String(id_direktebla_kosmo_query):
			if on_data['payload'].get('data'):
				var uuid = []
				for pars in on_data['payload']['data']['filteredUniversoObjekto']['edges']:
					uuid.append(pars['node']['uuid'])
				for objekt in Global.direktebla_objekto:
					if objekt.has('uuid'):
						if objekt['uuid'] in uuid:
							objekt['kosmo']=true
				Global.loading = true
			#обработали запрос и удалили обработанную запись
			Net.data_server.remove(i)
			#отключаем перехват сигнала
			Net.disconnect("input_data", self, "_on_data_start")
# warning-ignore:return_value_discarded
			Net.connect("input_data", self, "_on_data")
			Global.objektoj.clear()
			load_objektoj()
			#отображение и включение аудиоплеера
			if Global.status and Global.nickname:
				var gramofono = load("res://blokoj/gramofono/scenoj/gramofono.tscn")
				var gramofono_child = gramofono.instance()
				add_child(gramofono_child)
			print('загрузили данные')
			$CanvasLayer/UI/Taskoj/Window.mendo_informoj_projekto()
			if (Global.server or Global.autoload) and Global.realeco!=1:
				go_realeco()
		i += 1


# обработчик прихода постоянных данных
func _on_data():
	var i_data_server = 0
	for on_data in Net.data_server:
		if !on_data['payload']['data']:
			print('=== error ===',on_data)
		elif on_data['id'] == String(id_ligilo):
			# вошли в станцию
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].front()['node']['uuid']=\
				on_data['payload']['data']['redaktuUniversoObjektoLigiloj']['universoObjektojLigiloj']['uuid']
			id_ligilo = 0
			Net.data_server.remove(i_data_server)
		elif on_data['payload']['data'].get('filteredUniversoObjekto'):
			# загрузка объектов
			var i = 0
			for item in on_data['payload']['data']['filteredUniversoObjekto']['edges']:
				if item['node']['uuid'] == Global.direktebla_objekto[Global.realeco-2]['uuid']:
					# обновляем данные directebla по своему кораблю
					var kosmo = Global.direktebla_objekto[Global.realeco-2]['kosmo']
					Global.direktebla_objekto[Global.realeco-2]=item['node'].duplicate(true)
					Global.direktebla_objekto[Global.realeco-2]['kosmo'] = kosmo
				else:# свой корабль не добавляем в список
					Global.objektoj.append(item['node'].duplicate(true))
					Global.objektoj[i]['distance'] = 0
					i += 1
			Net.data_server.remove(i_data_server)
			if on_data['payload']['data']['filteredUniversoObjekto']['pageInfo']['hasNextPage']:
				load_objektoj(on_data['payload']['data']['filteredUniversoObjekto']['pageInfo']['endCursor'])
			else:
				if $"/root".get_node_or_null('space'):# если загружен космос
					$"/root".get_node('space').emit_signal("load_objektoj")# загружаем объекты космоса
				emit_signal("load_objekto")
		i_data_server += 1


func _on_Profilo_pressed():
# warning-ignore:return_value_discarded
	$CanvasLayer/UI/Popup_profilo.popup()
	$CanvasLayer/UI/Popup_profilo/profilo1/VBox.set_visible(true)

func _on_Objektoj_pressed():
	$CanvasLayer/UI/Objektoj/Window/VBox.set_visible(true)


func set_visible(visible: bool):
	$CanvasLayer/UI.visible = visible
	

func _on_Taskoj_pressed():
	$CanvasLayer/UI/Taskoj/Window/VBox.set_visible(true)

func CloseWindow():
	$CanvasLayer/UI/Taskoj/Window/VBox.set_visible(false)
	$CanvasLayer/UI/Objektoj/Window/VBox.set_visible(false)
	$CanvasLayer/UI/b_itinero/itinero/canvas/MarginContainer.set_visible(false)
	$CanvasLayer/UI/interago/interago/VBox.set_visible(false)


func reloadWindow():
	$CanvasLayer/UI/Taskoj/Window.mendo_informoj_projekto()
	Global.objektoj.clear()
	load_objektoj()


func on_cap():
	var mezo_cap = preload("res://kerno/menuo/resursoj/icons/tab4_3.png")
	$CanvasLayer/UI/mezo_regions.icon = mezo_cap
	$CanvasLayer/UI/real/real_cap_rect.set_visible(true)
	$CanvasLayer/UI/real/real_com_rect.set_visible(false)
	$CanvasLayer/UI/com/com_cap_rect.set_visible(true)
	$CanvasLayer/UI/com/com_real_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_com_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_real_rect.set_visible(false)
	$CanvasLayer/UI/Lbar.color = Color(0, 0.407843, 0.407843, 0.862745)
	$CanvasLayer/UI/Lbar3.self_modulate = Color(0, 0.407843, 0.407843, 0.862745)
	$CanvasLayer/UI/real/realLabel.set_visible(false)
	$CanvasLayer/UI/com/comLabel.set_visible(false)
	$CanvasLayer/UI/cap/capLabel.set_visible(true)
	$CanvasLayer/UI/romb.color = Color(0, 0.407843, 0.407843, 0.862745)
	# если объекта не будет в космосе, то загружать станцию
	if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
# warning-ignore:return_value_discarded
		get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')
	else:
# warning-ignore:return_value_discarded
		get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')


func _on_cap_pressed():
	if Global.loading: #здесь вызывать задержку надо
		var reload = false
		if Global.realeco!=3:
			Global.realeco = 3
			reload = true
			# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
			CloseWindow()
			on_cap()
		if reload:
			reloadWindow()
	else:
		print('Ещё не загружено')


func on_com():
	var mezo_com = preload("res://kerno/menuo/resursoj/icons/tab4_1.png")
	$CanvasLayer/UI/mezo_regions.icon = mezo_com
	$CanvasLayer/UI/real/real_cap_rect.set_visible(false)
	$CanvasLayer/UI/real/real_com_rect.set_visible(true)
	$CanvasLayer/UI/com/com_cap_rect.set_visible(false)
	$CanvasLayer/UI/com/com_real_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_com_rect.set_visible(true)
	$CanvasLayer/UI/cap/cap_real_rect.set_visible(false)
	$CanvasLayer/UI/Lbar.color = Color(0.333333, 0, 0.066667, 0.862745)
	$CanvasLayer/UI/Lbar3.self_modulate = Color(0.333333, 0, 0.066667, 0.862745)
	$CanvasLayer/UI/real/realLabel.set_visible(false)
	$CanvasLayer/UI/com/comLabel.set_visible(true)
	$CanvasLayer/UI/cap/capLabel.set_visible(false)
	$CanvasLayer/UI/romb.color = Color(0.333333, 0, 0.066667, 0.862745)
	# если объекта не будет в космосе, то загружать станцию
	if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
# warning-ignore:return_value_discarded
		get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')
	else:
# warning-ignore:return_value_discarded
		get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')
#		get_tree().change_scene("res://blokoj/kosmostacioj/ComKosmostacio.tscn")


func _on_com_pressed():
	if Global.loading: #здесь вызывать задержку надо
		var reload = false
		if Global.realeco!=2:
			Global.realeco = 2
			reload = true
			# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
			CloseWindow()
			on_com()
		if reload:
			reloadWindow()
	else:
		print('Ещё не загружено')


func on_real():
	var mezo_real = preload("res://kerno/menuo/resursoj/icons/tab4_2.png")
	$CanvasLayer/UI/mezo_regions.icon = mezo_real
	$CanvasLayer/UI/real/real_cap_rect.set_visible(false)
	$CanvasLayer/UI/real/real_com_rect.set_visible(false)
	$CanvasLayer/UI/com/com_cap_rect.set_visible(false)
	$CanvasLayer/UI/com/com_real_rect.set_visible(true)
	$CanvasLayer/UI/cap/cap_com_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_real_rect.set_visible(true)
	$CanvasLayer/UI/Lbar.color = Color(0.4, 0.6, 1, 0.862745)
	$CanvasLayer/UI/Lbar3.self_modulate = Color(0.4, 0.6, 1, 0.862745)
	$CanvasLayer/UI/real/realLabel.set_visible(true)
	$CanvasLayer/UI/com/comLabel.set_visible(false)
	$CanvasLayer/UI/cap/capLabel.set_visible(false)
	$CanvasLayer/UI/romb.color = Color(0.4, 0.6, 1, 0.862745)
	var err = get_tree().change_scene("res://blokoj/kosmostacioj/Kosmostacio.tscn")
	if err:
		print('ошибка смены сцены = ',err)


func _on_real_pressed():
	var reload = false
	if Global.realeco!=1:
		Global.realeco = 1
		reload = true
		# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
		CloseWindow()
		on_real()
# warning-ignore:return_value_discarded
	if reload:
		reloadWindow()


func _on_b_itinero_pressed():
	$CanvasLayer/UI/b_itinero/itinero/canvas/MarginContainer.set_visible(true)


func _on_ad_pressed():
	$CanvasLayer/UI/ad/ad_1/CanvasLayer/Margin.set_visible(true)


func _on_interago_pressed():
	if $CanvasLayer/UI/interago/interago/VBox.visible:
		$CanvasLayer/UI/interago/interago/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/interago/interago.print_button()
		$CanvasLayer/UI/interago/interago/VBox.set_visible(true)


func _on_eliro_button_up():
	$CanvasLayer/UI/eliro/eliro.popup_centered()


func _input(event):
	if event is InputEvent and Input.is_key_pressed(KEY_ESCAPE):
		$CanvasLayer/UI/eliro/eliro.popup_centered()


# запрашиваем объекты
func load_objektoj(after=""):
	var q = QueryObject.new()
	id_objekto = Net.get_current_query_id()
#	зугрузка объектов партиями, количество в партии указано в константе запроса
	Net.send_json(q.get_objekto_json(Net.statuso_laboranta, Net.tasko_tipo_objekto, Global.kubo, id_objekto, after))


func _on_komerco_button_up():
	$CanvasLayer/UI/komerco/komerco/VBox.set_visible(true)


func _on_konservejo_pressed():
	if $CanvasLayer/UI/konservejo/konservejo/VBox.visible:
		$CanvasLayer/UI/konservejo/konservejo/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/konservejo/konservejo/VBox.set_visible(true)




func _on_indicoj_pressed():
	if $CanvasLayer/UI/indicoj/indicoj/VBox.visible:
		$CanvasLayer/UI/indicoj/indicoj/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/indicoj/indicoj/VBox.set_visible(true)


func _on_items_pressed():
	if $CanvasLayer/UI/items/items_menuo.visible:
		$CanvasLayer/UI/items/items_menuo.set_visible(false)
	else:
		$CanvasLayer/UI/items/items_menuo.set_visible(true)

