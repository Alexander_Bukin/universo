extends MeshInstance

signal new_way_point(position)
# Called when the node enters the scene tree for the first time.
var dock = Quat()
func _ready():
# warning-ignore:return_value_discarded
	$tactical_label.connect("new_way_point",self,"set_way_point")
	pass

func set_way_point(position):
	emit_signal("new_way_point",position,dock)

# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_Area_input_event(camera, event, click_position, click_normal, shape_idx):
	if Input.is_action_just_pressed("left_click"):
		$menu.popup()
		if event is InputEventMouseButton and event.doubleclick:
			set_way_point($tactical_label.get_global_transform().origin)

func _on_Area_mouse_entered():
	var mat = get_surface_material(0)
	mat.flags_unshaded = true

func _on_Area_mouse_exited():
	var mat = get_surface_material(0)
	mat.flags_unshaded = false

