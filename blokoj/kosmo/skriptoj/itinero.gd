extends Control

# uuid выполняемого проекта
var projekto_itineroj_uuid
var projekto_mouse # проект запущен мышкой

# если маршрут создаётся списком, то это новый проект
# если мышкой по космосу после мышки - меняем задачу на новую в том же проекте
# если мышкой по космосу после проекта (несколько задач в списке), то закрываем проект и открываем новый проект

# маршрут движения управляемого объекта (список проектов с задачей маршрута)
var itineroj = [] 
#uuid_tasko - uuid задачи, когда уже летим
#uuid_celo - uuid цели полёта, если это объект 
#nomo - название объекта цели 
# координаты цели полёта
#			'koordinatoX':
#			'koordinatoY':
#			'koordinatoZ':
# distance - расстояние до цели полёта
# pozicio - в какой позиции должна находится задача
# transform - transform координат цели движения (лучше высчитать один раз при добавлении)
# kategorio - категория задачи объекта

# маршрут стоит на паузе
var itinero_pause = true

# запросы по задачам/проектам
const QueryObject = preload("../skriptoj/queries.gd")

# список id запросов, отправленных на сервер
var id_projekto # id запроса по созданию проекта
var id_taskoj # id запроса на список задач
var id_tasko # id запроса на одну задачу

func _ready():
	Global.fenestro_itinero = self
	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)


func _on_data():
	var i_data_server = 0
	for on_data in Net.data_server:
		if int(on_data['id']) == id_projekto:
			# запустили новый проект
			taskoj_to_server(on_data['payload']['data'])
			Net.data_server.remove(i_data_server)
			id_projekto = 0
		elif int(on_data['id']) == id_taskoj:
			# присвоить uuid задачам после ответа
			# uuid соответственно переданным задачам - по порядку
			var i = 0
			for datoj in on_data['payload']['data']['redaktuKreiUniversoTaskojPosedanto']['universoTaskoj']:
				itineroj[i]['uuid_tasko'] = datoj['uuid']
				i += 1
			id_taskoj = 0
			Net.data_server.remove(i_data_server)
		elif int(on_data['id']) == id_tasko:
			itineroj.front()['uuid_tasko'] = on_data['payload']['data']['redaktuUniversoTaskoj']['universoTaskoj']['uuid']
			id_tasko = 0
			Net.data_server.remove(i_data_server)
		i_data_server += 1

var step = 0 #шаг времени для обновления расстояния

func _physics_process(delta):
	step += delta
	if step>1:
		step = 0
		if Global.fenestro_kosmo:
			distance_to(Global.fenestro_kosmo.get_node("ship").translation)


func _on_Close_button_pressed():
	$"canvas/MarginContainer".set_visible(false)


func _resize(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$canvas/MarginContainer.rect_size += event.relative


func _drag(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$canvas/MarginContainer.rect_position += event.relative


# очистить список маршрута в окне маршрута
func purigi_fenestro():
	$"canvas/MarginContainer/VBoxContainer/ItemList".clear()


# вывести на экран окна весь список маршрута с предварительным очищением окна
func FillItemList():
	purigi_fenestro()
	# Заполняет список маршрутом
	for Item in itineroj:
		get_node("canvas/MarginContainer/VBoxContainer/ItemList").add_item('('+String(int(Item['distance']))+') '+Item['nomo'], null, true)


#пересчет дистанции до объектов в списке
func distance_to(trans):
	for obj in itineroj:
		obj['distance'] = trans.distance_to(Vector3(obj['koordinatoX'],
			obj['koordinatoY'],obj['koordinatoZ']))
	$'canvas/MarginContainer/VBoxContainer/ItemList'.clear()
	FillItemList()


# начинаем маршрут
func komenci_itinero():
	if Global.fenestro_kosmo:
		# отправляем в полёт
		vojkomenco()#начинаем движение
		if !itinero_pause:
			#запускаем таймер
			Global.fenestro_kosmo.get_node("timer").start()
			$canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero.disabled=true


# начинаем лететь
func _on_kom_itinero_pressed():
	if Global.fenestro_kosmo:
		projekto_mouse = false # проект не по мышке
		#создать проект
		#создать список задач на основе списка itineroj
		komenci_itinero()



# ткнули мышкой в космосе, отправились к точке в космосе
# отправили к конкретному объекту mouse=false
# добавить задачу в маршрут, а при отсутствии проекта - создать маршрут из одной точки
func okazigi_itinero(uuid_celo, nomo, koordinatoX, koordinatoY, 
	koordinatoZ, transform, distance, kategorio, mouse):
		# kategorio=Net.kategorio_movado
	if len(itineroj)>1:
		fermi_projekto_tasko()
	elif len(itineroj)>0:
		malmultigi_unua(mouse)
	if !transform:
		transform = Transform(Basis.IDENTITY, Vector3(koordinatoX,
			koordinatoY, koordinatoZ))
	if (distance == -1) and (Global.fenestro_kosmo):
		distance = Global.fenestro_kosmo.get_node("ship").translation.distance_to(Vector3(koordinatoX,
			koordinatoY,koordinatoZ))
	Global.fenestro_kosmo.get_node("ship").target_rot = Quat(Global.fenestro_kosmo.get_node("ship").transform.looking_at(transform.origin,Vector3.UP).basis) #запоминаем в какое положение надо установить корабль, чтобы нос был к цели. Это в кватернионах. ХЗ что это, но именно так вращать правильнее всего.
	add_itinero('', uuid_celo, nomo, koordinatoX, 
		koordinatoY, koordinatoZ, transform, distance, 
		kategorio, -1)
	vojkomenco()


# добавить точку маршрута 
func add_itinero(uuid_tasko, uuid_celo, nomo, koordinatoX, 
	koordinatoY, koordinatoZ, transform, distance, kategorio,
	pozicio):
	itineroj.append({
		'uuid_tasko':uuid_tasko,
		'uuid_celo':uuid_celo,
		'nomo':nomo,
		'koordinatoX':koordinatoX,
		'koordinatoY':koordinatoY,
		'koordinatoZ':koordinatoZ,
		'transform': transform,
		'distance':distance,
		'kategorio':kategorio,
		'pozicio':pozicio
	})
	get_node("canvas/MarginContainer/VBoxContainer/ItemList").add_item('('+String(int(distance))+') '+nomo, null, true)


# удаляем первую точку маршрута (выполнена)
# mouse - движение по мышке
func malmultigi_unua(mouse = false): # уменьшить первый
	# если первая точка задача с uuid - закрываем её
	# ставим задачу в "Закрыта" (Статус = 4)
	if itineroj.front()['uuid_tasko']:
		var q = QueryObject.new()
		var id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
		Net.send_json(Queries.finado_tasko(itineroj.front()['uuid_tasko'],Net.statuso_fermo,id))
	itineroj.pop_front()
#	а если это движение по мышке?? что бы продолжить тот же проект
	if mouse and projekto_mouse:
		FillItemList()
	elif len(itineroj)==0:# если была последняя точка, то закрываем проект
		fermi_projekto()
	else:
		FillItemList()


# отправляем на сервер закрытие проекта
#func send_fermi_projekto(projekto_uuid):
#	var q = QueryObject.new()
#	var id = Net.get_current_query_id()
#	Net.net_id_clear.push_back(id)
#	Net.send_json(q.finado_projeko_json(projekto_uuid,id))


# закрываем проект
func fermi_projekto():
	if projekto_itineroj_uuid:
#		send_fermi_projekto(projekto_itineroj_uuid)
		projekto_itineroj_uuid = ''
		projekto_mouse = true
	malplenigi_itinero()


# закрываем проект и первую (текущую) задачу
func fermi_projekto_tasko():
	if projekto_itineroj_uuid:
#		var q = QueryObject.new()
#		var id = Net.get_current_query_id()
#		Net.net_id_clear.push_back(id)
#		if itineroj.front()['uuid_tasko']:
#			Net.send_json(q.finado_projekto_tasko_json(projekto_itineroj_uuid,itineroj.front()['uuid_tasko'],id))
#		else:
#			Net.send_json(q.finado_projeko_json(projekto_itineroj_uuid,id))
		projekto_itineroj_uuid = ''
		projekto_mouse = true
	malplenigi_itinero()


# очищаем маршрут  clear_itinero
func malplenigi_itinero():
	itineroj.clear()
	itinero_pause = true
	FillItemList()
#	приводим кнопки в исходное состояние
	Global.fenestro_kosmo.get_node("timer").stop()
	$canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero.disabled=false
	$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next.disabled=true
	$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.disabled=true
	$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_clear.disabled=true
	$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.text='Пауза'


# поставить на паузу или отправить дальше
func _on_itinero_fin_pressed():
	if Global.fenestro_kosmo:
		#если в полёте
		if $canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero.disabled:
			if !itinero_pause:
				#останавливаем движение корабля
				$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.text='Далее'
				$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next.disabled=true
				itinero_pause = true
			else:
				$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.text='Пауза'
				#продолжаем движение корабля
				$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next.disabled=false
				itinero_pause = false


# кнопка сброса маршрута
func _on_itinero_clear_pressed():
	projekto_mouse = true
	if len(itineroj)==0:
		return 404
	if Global.fenestro_kosmo:
		#закрыть все задачи и проект
		fermi_projekto_tasko()


# пропустить текущую цель и лететь к следующей
func _on_itinero_next_pressed():
	if Global.fenestro_kosmo:
		Global.fenestro_kosmo.get_node("timer").stop()
		# закрываем текущую задачу
		malmultigi_unua()
		# переводим следующую задачу в работу
		vojkomenco()


#передача данных на сервер при отправке корабля по первой цели
func vojkomenco(): # нача́ло доро́ги
	if len(itineroj)==0:
		return 404
	var q = QueryObject.new()
	# отправляем на сервер маршрут
	if !projekto_itineroj_uuid:#если проекта нет, то создаём
		# создание проекта с задачами идёт в два этапа:
		# 1 - создаём запись проекта и получаем его uuid
		# 2- создаём задачи к проекту после ответа сервера
		# цель маршрута берём из itineroj
		id_projekto = Net.get_current_query_id()
		var count_itineroj=len(itineroj)-1
		Net.send_json(q.instalo_projekto_json(
			Global.direktebla_objekto[Global.realeco-2]['uuid'], # uuid объекта управления
			Global.fenestro_kosmo.get_node("ship").translation.x, #kom_koordX
			Global.fenestro_kosmo.get_node("ship").translation.y, #kom_koordY
			Global.fenestro_kosmo.get_node("ship").translation.z, #kom_koordZ
			itineroj[count_itineroj]['koordinatoX'], #fin_koordX
			itineroj[count_itineroj]['koordinatoY'], #fin_koordY
			itineroj[count_itineroj]['koordinatoZ'], #fin_koordZ
			id_projekto
		))
	else:#проект есть, изменяем задачу
		# изменяем цель проекта
		var id = Net.get_current_query_id()
		if itineroj.front()['uuid_tasko']: # если есть uuid задачи
			print('===фишгня какая то - нет такой функции')
			Net.send_json(q.instalo_tasko_uuid_koord_json(
#				если задача зарегистрирована на сервере, то мы должны передать её uuid
				itineroj.front()['uuid_tasko'], 
				Global.fenestro_kosmo.get_node("ship").translation.x, #kom_koordX
				Global.fenestro_kosmo.get_node("ship").translation.y, #kom_koordY
				Global.fenestro_kosmo.get_node("ship").translation.z, #kom_koordZ
				2, # берём в работу
				id
			))
		else: #создаём новую задачу
			id_tasko = id
			itineroj.front()['uuid_tasko'] = id_tasko
			Net.send_json(q.instalo_tasko_koord_json(
	#			если задача зарегистрирована на сервере, то мы должны передать её uuid
				Global.direktebla_objekto[Global.realeco-2]['uuid'], 
				projekto_itineroj_uuid, 
				Global.fenestro_kosmo.get_node("ship").translation.x, #kom_koordX
				Global.fenestro_kosmo.get_node("ship").translation.y, #kom_koordY
				Global.fenestro_kosmo.get_node("ship").translation.z, #kom_koordZ
				itineroj.front()['koordinatoX'], #fin_koordX
				itineroj.front()['koordinatoY'], #fin_koordY
				itineroj.front()['koordinatoZ'], #fin_koordZ
				id_tasko
			))
	blokado_butono()


# блокирование кнопок
func blokado_butono():
	get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero").disabled=true
	get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next").disabled=false
	get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin").disabled=false
	get_node("canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_clear").disabled=false


# записав проект в базу, получили его uuid и теперь передаём на сервер его задачи
func taskoj_to_server(on_data):
	projekto_itineroj_uuid = on_data['redaktuUniversoProjekto']['universoProjekto']['uuid']
	# теперь создаём задачу с координатами
	var q = QueryObject.new()
	var ship = Global.fenestro_kosmo.get_node("ship")
	Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = ship.translation.x
	Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = ship.translation.y
	Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = ship.translation.z
	Global.direktebla_objekto[Global.realeco-2]['rotationX'] = ship.rotation.x
	Global.direktebla_objekto[Global.realeco-2]['rotationY'] = ship.rotation.y
	Global.direktebla_objekto[Global.realeco-2]['rotationZ'] = ship.rotation.z
	id_taskoj = Net.get_current_query_id()
	Net.send_json(q.instalo_tasko_posedanto_koord(
			ship.uuid, 
			projekto_itineroj_uuid, 
			ship.translation.x, #kom_koordX
			ship.translation.y, #kom_koordY
			ship.translation.z, #kom_koordZ
			itineroj,
			id_taskoj
	))


# Добавить блокировку кнопок управления маршрутом
# редактировать маршрут
#добавить кругом содержание массива itineroj
#такие как 
#	transform, distance, 
func sxangxi_itinero(projekto, tasko):
	if !Global.fenestro_kosmo: # если космос не загружен, то и маршрут не загружаем
		return false
	var sxipo = Global.fenestro_kosmo.get_node("ship")
	var nomo = ''
	if sxipo.uuid != tasko['objekto']['uuid']:
		nomo = tasko['objekto']['nomo']['enhavo']
	var vector = Vector3(tasko['finKoordinatoX'],
		tasko['finKoordinatoY'], 
		tasko['finKoordinatoZ'])
	if projekto['uuid']==projekto_itineroj_uuid: # изменение по задаче в текущем проекте
		var new_tasko = true # признак необходимости новой задачи
		var pos = 0 # номер позиции в списке задач
		for it in itineroj:# проходим по всем задачам
			if tasko['uuid'] == String(it['uuid_tasko']): # нашли соответствующую задачу
				new_tasko = false
				if tasko['statuso']['objId']==Net.statuso_nova: # новая -  в очередь выполнения 
					it['koordinatoX'] = tasko['finKoordinatoX']
					it['koordinatoY'] = tasko['finKoordinatoY']
					it['koordinatoZ'] = tasko['finKoordinatoZ']
				elif tasko['statuso']['objId']==Net.statuso_laboranta: # в работе
					# задача должна быть первой в списке
					if pos: # если не первая
						itineroj.remove(pos)
						itineroj.push_front({
							'uuid_tasko':tasko['uuid'],
							'koordinatoX':tasko['finKoordinatoX'],
							'koordinatoY':tasko['finKoordinatoY'],
							'koordinatoZ':tasko['finKoordinatoZ'],
							'uuid_celo':tasko['objekto']['uuid'],
							'nomo': nomo,
							'pozicio': tasko['pozicio'],
							'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
							'transform': Transform(Basis.IDENTITY, vector),
							'distance': sxipo.translation.distance_to(vector)
						})
					else:
						it['koordinatoX'] = tasko['finKoordinatoX']
						it['koordinatoY'] = tasko['finKoordinatoY']
						it['koordinatoZ'] = tasko['finKoordinatoZ']
					# отправляем корабль на уточнённые координаты, а точнее поворачиваем
					sxipo.rotate_start()
					itinero_pause = false
				elif tasko['statuso']['objId']==Net.statuso_fermo: # закрыта
					#удаляем из списка
					itineroj.remove(pos)
				elif tasko['statuso']['objId']==Net.status_pauzo: # приостановлена
					itinero_pause = true
			pos += 1
		if new_tasko: # добавляем новую задачу в проект
			if tasko['statuso']['objId']==Net.statuso_nova: # новая -  в очередь выполнения 
				# нужно выстроить по очерёдности
				pass
				pos = 0
				var pozicio = false
				for it in itineroj:# проходим по всем задачам и находим нужную позицию
					if it['pozicio']>tasko['pozicio']: # вставляем перед данной записью
						itineroj.insert(pos, {
							'uuid_tasko':tasko['uuid'],
							'koordinatoX': tasko['finKoordinatoX'],
							'koordinatoY': tasko['finKoordinatoY'],
							'koordinatoZ': tasko['finKoordinatoZ'],
							'pozicio': tasko['pozicio'],
							'uuid_celo':tasko['objekto']['uuid'],
							'nomo': nomo,
							'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
							'transform': Transform(Basis.IDENTITY, vector),
							'distance': sxipo.translation.distance_to(vector)
						})
						pozicio = true
						break
					pos += 1
				if !pozicio: # позиция не найдены, добавляем в конце
					itineroj.push_pop({
						'uuid_tasko':tasko['uuid'],
						'koordinatoX': tasko['finKoordinatoX'],
						'koordinatoY': tasko['finKoordinatoY'],
						'koordinatoZ': tasko['finKoordinatoZ'],
						'pozicio': tasko['pozicio'],
						'uuid_celo':tasko['objekto']['uuid'],
						'nomo': nomo,
						'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
						'transform': Transform(Basis.IDENTITY, vector),
						'distance': sxipo.translation.distance_to(vector)
					})
			elif tasko['statuso']['objId']==Net.statuso_laboranta: # в работе
					# задача должна быть первой в списке
				itineroj.push_front({
					'uuid_tasko':tasko['uuid'],
					'koordinatoX':tasko['finKoordinatoX'],
					'koordinatoY':tasko['finKoordinatoY'],
					'koordinatoZ':tasko['finKoordinatoZ'],
					'pozicio': tasko['pozicio'],
					'uuid_celo':tasko['objekto']['uuid'],
					'nomo': nomo,
					'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
					'transform': Transform(Basis.IDENTITY, vector),
					'distance': sxipo.translation.distance_to(vector)
				})
					# отправляем корабль на уточнённые координаты, а точнее поворачиваем
				sxipo.rotate_start()
				itinero_pause = false
			elif tasko['statuso']['objId']==Net.status_pauzo: # приостановлена
				itineroj.push_front({
					'uuid_tasko':tasko['uuid'],
					'koordinatoX': tasko['finKoordinatoX'],
					'koordinatoY': tasko['finKoordinatoY'],
					'koordinatoZ': tasko['finKoordinatoZ'],
					'pozicio': tasko['pozicio'],
					'uuid_celo':tasko['objekto']['uuid'],
					'nomo': nomo,
					'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
					'transform': Transform(Basis.IDENTITY, vector),
					'distance': sxipo.translation.distance_to(vector)
				})
				itinero_pause = true
	else: # первая запись нового проекта
		if not(tasko['statuso']['objId']==Net.statuso_fermo):
			malplenigi_itinero()
			projekto_itineroj_uuid = projekto['uuid']
			itineroj.push_back({
				'uuid_tasko':tasko['uuid'],
				'koordinatoX': tasko['finKoordinatoX'],
				'koordinatoY': tasko['finKoordinatoY'],
				'koordinatoZ': tasko['finKoordinatoZ'],
				'pozicio': tasko['pozicio'],
				'uuid_celo':tasko['objekto']['uuid'],
				'nomo': nomo,
				'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
				'transform': Transform(Basis.IDENTITY, vector),
				'distance': sxipo.translation.distance_to(vector)
			})
			if tasko['statuso']['objId']==Net.statuso_laboranta: # в работе
				sxipo.rotate_start()
				itinero_pause = false
			else:
				itinero_pause = true
	if len(itineroj)>0:
		blokado_butono()

