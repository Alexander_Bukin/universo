extends Tree


func _ready():
	var root = self.create_item()
	root.set_text(0, 'Космические корабли')

	var child1 = self.create_item(root)
	child1.set_text(0, 'Грузовые космические корабли')

	var subchild1 = self.create_item(child1)
	subchild1.set_text(0, 'Типовые грузовые космические корабли')
