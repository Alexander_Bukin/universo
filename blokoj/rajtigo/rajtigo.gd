extends Control


func _ready():
	if Global.logs:
		print('запуск res://blokoj/rajtigo/rajtigo.tscn')
	# счтываем файл настроек и проверяем - может запущен серверный клиент
	var config = ConfigFile.new()
	var err = config.load("res://settings.cfg")
	if err == OK:
		if Global.autoload:
			var password = config.get_value("aliro", "password")
			var login = config.get_value("aliro", "uzanto")
			$"Start_page_menuo/CanvasLayer/UI/Popup/for_auth/VBox/body_texture/LoginButton".eniri_uzanto(login, password)
		elif Global.server:
			$"Start_page_menuo/CanvasLayer/UI/Popup/for_auth/VBox/body_texture/LoginButton".eniri_server()


# Вызывается при открытии сцены
func _enter_tree():
	# Прячем глобальную сцену с меню
	Title.set_visible(false)
	
	# Скрываем символы в поле для ввода пароля
	$"Start_page_menuo/CanvasLayer/UI/Popup/for_auth/VBox/body_texture/your_password".set_secret(true)
	

# Вызывается при закрытии сцены
func _exit_tree():
	# Показываем глобальную сцену с меню
	Title.set_visible(true)
	

